package com.curso.controller;

import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Descarga extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4958395027080562682L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		descargar(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		descargar(req, resp);
	}

	private void descargar(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		// Descarga im�genes
		/*
		 * Bloque de c�digo que env�a una im�gen a trav�s del response, haciendo
		 * useo de MIME.
		 */
		// if(req.getParameter("accion").equals("Descargar")){}

		String img = "";
		String strImagen = req.getParameter("imagenes");
		if (strImagen.equals("Daenerys")) {
			img = "daenerys";
		} else if (strImagen.equals("Arya")) {
			img = "arya-stark";
		} else if (strImagen.equals("Maergaery")) {
			img = "margaery-tyrell";
		}

			resp.setContentType("image/png");
			ServletOutputStream outputStream = resp.getOutputStream();
			FileInputStream fileInputStream = new FileInputStream(
					"C:\\Users\\Froy\\workspace_EE\\ApacheShiroWebX\\WebContent\\WEB-INF\\media\\img\\"
							+ img + ".png");
			byte[] b = new byte[1024];
			resp.setHeader("Content-Disposition", "attachment; filename=\""
					+ img + ".png" + "\"");
			while (fileInputStream.read(b) != -1) {
				outputStream.write(b);
			}
			if (true) {
				return;
			}
		// Descarga im�genes
	}
}
