package com.curso.controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.subject.Subject;

import com.curso.model.SubjectUserInfo;

public class ShiroTest extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 934886343483647516L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		accion(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		accion(req, resp);
	}

	private void accion(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		HttpSession session = req.getSession();
		session.removeAttribute("errorMensaje");
		String user;
		String password;
		Subject subject = SubjectUserInfo.obtenerSubject(this);
		user = req.getParameter("user");
		password = req.getParameter("password");

		if (user.trim().length() == 0) {
			session.setAttribute("errorMensaje",
					"El usuario ingresado no tiene datos ");
		}
		if (password.trim().length() == 0) {
			session.setAttribute("errorMensaje",
					session.getAttribute("errorMensaje")
							+ " El password ingresado no tiene datos");
		}
		if (user.trim().length() == 0 || password.trim().length() == 0) {
			enviarRespuesta(resp);
			return;
		}

		SubjectUserInfo info = SubjectUserInfo.loggear(subject, user, password);
		// SubjectUserInfo info = SubjectUserInfo.loggear(subject, "admin",
		// "admin2");
		if (info.isStatus()) {
			subject.getSession().setAttribute("Mensaje",
					"Se conectará data con " + subject.getPrincipal());
			subject.getSession()
					.setAttribute("Usuario", subject.getPrincipal());
		} else {
			// System.out.println("ShiroTest.doGet();"+info.getMensaje());
			session.setAttribute("errorMensaje", info.getMensaje());
		}
		System.out.println("ShiroTest.doGet();" + info);
		enviarRespuesta(resp);
	}

	public void enviarRespuesta(HttpServletResponse resp) throws IOException {
		resp.sendRedirect("index.jsp");
	}

}
