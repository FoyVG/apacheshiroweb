package com.curso.model;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.http.HttpServlet;

public class SubjectUserInfo {
	private boolean status;
	private String mensaje;

	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public SubjectUserInfo(boolean status, String mensaje) {
		super();
		this.status = status;
		this.mensaje = mensaje;
	}
	public SubjectUserInfo() {
		super();
	}
	public static Subject obtenerSubject(HttpServlet servlet){
	  SecurityUtils.setSecurityManager(WebUtils.getRequiredWebEnvironment(servlet.getServletContext()).getSecurityManager());
	  return SecurityUtils.getSubject();
	}
	public static SubjectUserInfo loggear(Subject sujeto, String usuario, String password){
		SubjectUserInfo subjectUserInfo = new SubjectUserInfo();
		if (!sujeto.isAuthenticated()) {
            UsernamePasswordToken token = new UsernamePasswordToken(usuario, password);
            token.setRememberMe(true);
            subjectUserInfo.setStatus(false);
            try {
            	sujeto.login(token);
            	subjectUserInfo.setStatus(true);
//            	System.out.println(token.getPrincipal());
//            	System.out.println(token.getUsername());
//            	System.out.println(token.getPassword());
            	subjectUserInfo.setMensaje("Usuario [" + sujeto.getPrincipal() + "] loggeado exitosamente.");
            } catch (UnknownAccountException uae) {
            	subjectUserInfo.setMensaje("No existe el usuario: " + token.getPrincipal());
            } catch (IncorrectCredentialsException ice) {
            	subjectUserInfo.setMensaje("Password para la cuenta: " + token.getPrincipal() + " es incorrecta!");
            } catch (LockedAccountException lae) {
            	subjectUserInfo.setMensaje("La cuenta para el usuario: " + token.getPrincipal() + " is locked. Please contact your administrator to unlock it.");
            } catch (AuthenticationException ae) {         
            	subjectUserInfo.setMensaje("La cuenta para el usuario: " + token.getPrincipal() + " no se pudo autentificar.");
            }
        } else{
        	subjectUserInfo.setStatus(true);
        	subjectUserInfo.setMensaje("Sujeto [" + sujeto.getPrincipal() + "] este es el usuario existente en la session.");
        }
		return subjectUserInfo;
	}
	@Override
	public String toString() {
		return "SubjectUserInfo [status=" + status + ", mensaje=" + mensaje
				+ "]";
	}
}
