<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@taglib prefix="fmt" uri="/WEB-INF/tld/fmt.tld"%>
<%@taglib prefix="shiro" uri="/WEB-INF/tld/shiro.tld"%>
<!DOCTYPE HTML >

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ejemplo de Apache Shiro</title>
<link rel=StyleSheet href="/ApacheShiroWebX/CSS/style.css"
	type="text/css" />
<script type="text/javascript" src="/ApacheShiroWebXs/JS/jquery-3.3.1.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<h1>Ejemplo de Apache Shiro</h1>
	<div id="login">
		<c:if test="${Usuario == null }">
			<form action="./ShiroTest" method="post">
				<div>
					<label for="user">Usuario:</label> <input type="text" id="user"
						name="user" />
				</div>
				<div>
					<label for="password">Contraseña:</label> <input type="password"
						id="pass" name="password" />
				</div>
				<div>
					<input type="submit" value="Login">
				</div>
			</form>
		</c:if>
		<c:if test="${Usuario != null }">
			<h2>
				Bienvenido: <a href="">${Usuario }</a>
			</h2>
			<a href="./logout">Logout</a>
		</c:if>
	</div>
	<div id="bodyResponse">
		<!-- <a href="./ShiroTest">Login</a>
    <a href="./logout">Logout</a> -->
		<c:if test="${errorMensaje != null}">
			<h1 style="color: red">${errorMensaje}</h1>
		</c:if>
		<c:if test="${(errorMensaje == null)}">
			<shiro:hasRole name="administrador">
				<h2>${Mensaje}</h2>
			</shiro:hasRole>
			<shiro:hasRole name="dba">
				<h2>Usted cuenta con los siguientes permisos:</h2>
				<h2>El usuario tiene asignado el rol de DBA</h2>
			</shiro:hasRole>

			<shiro:hasPermission name="crear">
				<h2>Usted tiene el permiso para crear</h2>
			</shiro:hasPermission>

			<shiro:hasPermission name="base_de_datos">
				<h2>Usted tiene el permiso para base de datos</h2>
			</shiro:hasPermission>

			<shiro:hasPermission name="descargar imagenes">
				<h2>Tienes permisos para descargar imágenes</h2>
				<p>Seleciona la imágen que deseas descargar</p>
				<form action="./DescargaImg" method="post">
					<select id="imagenes" name="imagenes">
						<option value="Daenerys">Daenerys</option>
						<option value="Maergaery">Maergaery</option>
						<option value="Arya">Arya</option>
					</select>
					<input type="submit" value="Descargar">
					<input name="accion" type="hidden" value="Descargar">
				</form>
			</shiro:hasPermission>
		</c:if>
	</div>
</body>
</html>
